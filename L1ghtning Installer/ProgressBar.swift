//
//  ProgressBar.swift
//  L1ghtning
//
//  Created by Luke Chambers on 11/24/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import SwiftUI

struct ProgressBar: View {
    @Binding var progress: CGFloat
    
    @State var isShowing = false
    
    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .leading) {
                Rectangle()
                    .foregroundColor(.gray)
                    .opacity(3)
                    .frame(width: geometry.size.width, height: geometry.size.height)
                Rectangle()
                    .foregroundColor(.blue)
                    .frame(width: self.isShowing ? geometry.size.width * (self.progress / 100) : 0,
                           height: geometry.size.height)
                    .animation(.linear(duration: 0))
            }
            .onAppear {
                self.isShowing = true
            }
            .cornerRadius(geometry.size.height / 2)
        }
        .frame(height: 5)
    }
}

struct ProgressBar_Previews: PreviewProvider {
    static var previews: some View {
        ProgressBar(progress: .constant(25))
    }
}
